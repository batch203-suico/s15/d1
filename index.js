console.log("Hello, world!");
console. log ("Hello, Batch 203!")
console.
log
(
	"Hello, Everyone!"
)
// Comments:

// This is a single line comment ctrl + /

/*
	This is a 
	Multi line comment
	ctrl + shft + /
*/

// Syntax and Statements

// Statements in programming are instructions that we tell the computer to perform

// Syntax in programming it is the set of rules that describes how statements must be considered

// Variables
/*
	-it is used to contain data

	- Syntax in declaring variables
	 - let/const variablesName;
*/

let myVAriable = "Hello";

console.log(myVAriable);

/*console.log(hello);

let hello*/

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of you choosing and use the assignment operator (=) to assign a value.
		2. Variables names should start with a lowercase character, use camelcase for multiple words.
		3. for constant variables, use the 'const' keyword.
		4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion 
		5. Never name a variable starting with numbers

*/

/* Declaring a variable with initial value
	 let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";

let productPrice = 18999;

console.log(productPrice);

const interest = 3.539;

// ---------------------

//var(ES1) vs let/const(ES6)


// Multiple variable declarations
let productCode = "DC17";
let productBrand = "Dell";
console.log(productCode, productBrand);

//Using a variable with a reserved keyword;
// const let = "hello";
// console.log(let); // error: cannot used reserved keywords as variable name.

// [SECTION] Data Types
// Strings
// Strings are series of characters that are create a word, a phrase, a sentence or anything related to creating text.
// Strings in JavaScript a single ('') or double ("")qoute.
let country = 'Philippines';
let province = "Metro Manila";

// Concatenation Strings
// Multiple string values can be combined to create a single string using the "+" symbol.
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character(\)
// "\n" refers to creating newline or set the text to next line.
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early."
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount)

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade)

// Exponential Notation
let planetDistance =2e10;
console.log(planetDistance);

// Combine number and strings
console.log("John's grade last quarter"+grade);

// Boolean 
// true/false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// it is used to store multiple values with similar data type.
// Syntax:
	//let/constv arrayName = [elementA, elementB, elementC,....]
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	// different data types
	// Storing different data types inside an array is not recommended because it will not make sense to in the context of programming.
	let detailes = ["John", "Smith", 32, true];
	console.log(detailes);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items.
// Syantax
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}

	*/ 

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address:{
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person);

	// Create abstract object
	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}

	console.log(myGrades);

	console.log(typeof grades);

	// Contstant Objects  and Arrays
	// We cannot reassign the value of the variable but we can change the elements of the constant array. 
	const anime = ["one piece", "one punch man", "attack on titan",]
	console.log(anime);
	anime[0] = ["demon slayer"]


	console.log(anime);

	// Null 
	// it is used to intentionally express the absence of the value in a variable decalaration/initialization.

	let spouse = null;

	console.log(spouse);

	let myNumber = 0; //number
	let myString = ""; //string

	// undefined
	// Represents the state of a variable that has been declared but without an assigned value.
	let fullName;
	console.log(fullName);




















